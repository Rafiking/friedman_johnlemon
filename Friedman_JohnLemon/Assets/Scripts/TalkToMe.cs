﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TalkToMe : MonoBehaviour
{
    public GameObject Wall;

    public Text textbox;

    public string toSay;

    public string sayThanks;

    public bool IsGhost;

    public GameObject MyItem;

    public GameObject NextGhost;

    public int ghostnumber;

    public AudioClip clip;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            textbox.gameObject.SetActive(true);
            textbox.text = toSay;

            if (IsGhost)
            {

                PlayerMovement Pmove = other.gameObject.GetComponent<PlayerMovement>();
                print("Items:" + Pmove.Items);
                if (Pmove.Items >= ghostnumber)
                {
                    Destroy(Wall);
                    textbox.gameObject.SetActive(true);
                    textbox.text = sayThanks;
                    AudioSource.PlayClipAtPoint(clip, gameObject.transform.position);
                }
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            textbox.gameObject.SetActive(false);
        }
    }


    private void OnDestroy()
    {
        textbox.gameObject.SetActive(false);
    }
}
